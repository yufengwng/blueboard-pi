#!/usr/bin/python2.7

import bluetooth
import dbus
import evdev
import keymap
import os
import sys
import time
import uuid


class BluetoothManager(object):

    # Ports for control and interrupt
    CTRL_PORT = 17
    INTR_PORT = 19

    def __init__(self):
        # Configure bluetooth device to be keyboard
        os.system("hciconfig hci0 class 0x002540")
        os.system("hciconfig hci0 piscan")

        # Define server sockets for communication
        self.scontrol = bluetooth.BluetoothSocket(bluetooth.L2CAP)
        self.sinterrupt = bluetooth.BluetoothSocket(bluetooth.L2CAP)

        # Bind sockets to a port
        self.scontrol.bind(('', BluetoothManager.CTRL_PORT))
        self.sinterrupt.bind(('', BluetoothManager.INTR_PORT))
        print("Control bound to port {}".format(BluetoothManager.CTRL_PORT))
        print("Interrupt bound to port {}".format(BluetoothManager.INTR_PORT))

        # Setup for service advertising via dbus
        self.bus = dbus.SystemBus()
        self.uuid = str(uuid.uuid1())
        self.manager = dbus.Interface(self.bus.get_object('org.bluez', '/org/bluez'), 'org.bluez.ProfileManager1')

        # Read in the service record
        with open("./sdp_record.xml", "r") as f:
            self.service_record = f.read()
        print("Done reading sdp_record.xml")

    def listen(self):
        # Advertise service record
        opts = {'ServiceRecord': self.service_record}
        self.manager.RegisterProfile('/', self.uuid, opts)
        print("Service record added")

        # Listen on server sockets with 1 connection limit each
        self.scontrol.listen(1)
        self.sinterrupt.listen(1)
        print("Waiting for connection...")

        self.ccontrol, self.cinfo = self.scontrol.accept()
        print("Got a connection on the control channel from " + self.cinfo[0])

        self.cinterrupt, self.cinfo = self.sinterrupt.accept()
        print("Got a connection on the interrupt channel from " + self.cinfo[0])

    def send_input(self, report):
        # Convert keyboard input report to hex string
        hex_str = ''
        for element in report:
            if type(element) == list:
                # Convert bit array to a byte value
                bin_str = ''.join(map(str, element))
                hex_str += chr(int(bin_str, base=2))
            else:
                hex_str += chr(element)
        self.cinterrupt.send(hex_str)


class KeyboardEmulator(object):

    def __init__(self):
        self.state = [
                0xA1,   # This is an input report
                0x01,   # Usage report = keyboard
                # Bit array for Modifier keys
                [0,     # Right Super
                 0,     # Right ALT
                 0,     # Right Shift
                 0,     # Right Control
                 0,     # Left Super
                 0,     # Left ALT
                 0,     # Left Shift
                 0],    # Left Control
                0x00,   # Vendor reserved
                0x00,   # Rest is space for 6 keys
                0x00,
                0x00,
                0x00,
                0x00,
                0x00]

        # Get the local keyboard device
        while True:
            try:
                self.dev = evdev.InputDevice('/dev/input/event0')
                break
            except OSError:
                print("Keyboard not found, waiting 3 seconds before retrying")
                time.sleep(3)
        print("Found a keyboard")

    def change_state(self, event):
        evdev_code = evdev.ecodes.KEY[event.code]
        modkey = keymap.modkey(evdev_code)
        if modkey > 0:
            # Toggle the modkey state
            active = self.state[2][modkey]
            self.state[2][modkey] = 0 if active else 1
        else:
            hex_key = keymap.convert(evdev_code)
            for i in range(4, 10):
                if self.state[i] == hex_key and event.value == 0:
                    self.state[i] = 0x00
                elif self.state[i] == 0x00 and event.value == 1:
                    self.state[i] = hex_key
                    break

    def event_loop(self, bt_man):
        print('Listening for key events...')
        for event in self.dev.read_loop():
            if event.type == evdev.ecodes.EV_KEY and event.value < 2:
                self.change_state(event)
                bt_man.send_input(self.state)


if __name__ == "__main__":
    # Need to be root user to run
    if not os.getuid() == 0:
        sys.exit("Must be run as root user")

    bt_man = BluetoothManager()
    bt_man.listen()

    kb_emu = KeyboardEmulator()
    kb_emu.event_loop(bt_man)

